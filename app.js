'use strict';

// Dependencies
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var dotenv = require('dotenv');
var session = require('express-session');
var passport = require('passport');

var errorHandler = require('errorhandler');
var fs = require('fs');
var moment = require('moment');
var filesize = require('filesize');
var path = require('path');


// Load environment variables from .env file, where API keys and passwords are configured.
dotenv.load({
  path: '.env'
});


var PORT = process.env.PORT || 8080;


// Create Express server
var app = express();


// Connect to MongoDB
mongoose.connect(process.env.MONGODB);
mongoose.connection.on('error', function() {
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});


// Express configuration
app.use('/static', express.static(__dirname + '/static'));
app.use('/uploads', express.static(__dirname + '/uploads'));
app.use(favicon(path.join(__dirname + '/static/favicon.ico')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(session({
  name: 'session',
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: {
    path: '/project-manager'
  }
}));
app.locals.moment = require('moment');
app.locals.filesize = require('filesize');

var passportConfig = require('./config/passport');

app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next) {
  res.locals.user = req.user;
  next();
});


// Controllers (route handlers)
var authenticationController = require('./controllers/project-manager/auth');
var projectManagerController = require('./controllers/project-manager/routes');
var publicSiteController = require('./controllers/public-site');


// Public site routes
app.get('/', publicSiteController.getIndex);
app.post('/', publicSiteController.postContact);

// Project manager auth routes
app.get('/project-manager/login', authenticationController.getLogin);
app.post('/project-manager/login', authenticationController.postLogin);
app.get('/project-manager/signup', authenticationController.getSignup);
app.post('/project-manager/signup', authenticationController.postSignup);
app.get('/project-manager/logout', authenticationController.getLogout);

// Project manager routes
app.get('/project-manager', projectManagerController.getIndex);
app.get('/project-manager/projects-list', passportConfig.isAuthenticated, projectManagerController.getProjectsList);
app.get('/project-manager/projects-add', passportConfig.isAuthenticated, projectManagerController.getAddProject);
app.post('/project-manager/projects-add', passportConfig.isAuthenticated, projectManagerController.postAddProject);
app.get('/project-manager/projects/:id', passportConfig.isAuthenticated, projectManagerController.getProjectView);
app.put('/project-manager/projects/:id', passportConfig.isAuthenticated, projectManagerController.putProjectView);
app.delete('/project-manager/projects/:id', passportConfig.isAuthenticated, projectManagerController.deleteProject);


// Error Handler
app.use(errorHandler());


// 404 Handler
app.use(function(req, res, next) {
  res.status(404).send('Resource not found!');
});


// Start Express server
app.listen(PORT, function() {
  console.log(`Server listening on port ${PORT}...`);
});