'use strict';

$(function() {
  $('.header-nav-link').click(function() {
    $('html, body').animate({
      scrollTop: $("#" + this.dataset.target).offset().top
    }, 500);
  });

  $('.header-logo').click(function() {
    $("html, body").animate({
      scrollTop: 0
    }, 500);
    return false;
  });

  var stickyHeader = new Waypoint({
    element: $('#stickyHeaderTrigger'),
    handler: function(direction) {
      if (direction === 'down') {
        $('header').addClass('stuck');
      } else {
        $('header').removeClass('stuck');
      }
    },
    offset: 200
  });

  var scrollToTop = new Waypoint({
    element: $('#scrollToTop'),
    handler: function(direction) {
      if (direction === 'down') {
        $('.scroller').removeClass('hidden');
      } else {
        $('.scroller').addClass('hidden');
      }
    },
    offset: '10%'
  });

  $('.scroller').click(function() {
    $("html, body").animate({
      scrollTop: 0
    }, 700);
  });

  $('#myWorksModal').on('show.bs.modal', function(event) {
    var trigger = $(event.relatedTarget);
    var source = trigger.data('source');
    var name = trigger.data('title');
    var modal = $(this);
    modal.find('.img-responsive').attr('src', source);
    modal.find('.project-title').html(name);
  });

  $('.message-sender').submit(function(event) {
    event.preventDefault();

    $.ajax({
      url: '/',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        name: $('input[name="name"]').val(),
        email: $('input[name="email"]').val(),
        subject: $('input[name="subject"]').val(),
        message: $('textarea[name="message"]').val()
      })
    }).done(function(response) {
      alert(response);
    });
  });

  $('#currentYear').html(new Date().getFullYear());
});