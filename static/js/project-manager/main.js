'use strict';

$(function() {
  $('.btn-delete-project').click(function() {
    var projectId = $(this).attr('data-projectId');
    var projectElement = $(this).closest('.project-container');

    if (confirm('Are you sure you want to delete this project?')) {
      $.ajax({
        url: '/project-manager/projects/' + projectId,
        type: 'DELETE'
      }).done(function(response) {
        alert(response);
        projectElement.fadeOut(500);
      });
    }
  });

  $('.btn-update-project').click(function(event) {
    event.preventDefault();
    var projectId = $('#project-id').text();
    console.log($('#project-description').val());

    $.ajax({
      url: '/project-manager/projects/' + projectId,
      type: 'PUT',
      contentType: 'application/json',
      data: JSON.stringify({
        name: $('#project-name').val(),
        category: $('#project-category').val(),
        description: $('#project-description').val()
      })
    }).done(function(response) {
      location.href = '/project-manager/projects-list';
    });
  });

  $('[data-toggle="tooltip"]').tooltip();
});