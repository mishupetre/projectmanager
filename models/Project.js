'use strict';

var mongoose = require('mongoose');

var projectSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true
  },
  category: String,
  description: String,
  fileName: String,
  path: String,
  size: String,
  addedOn: String,
  thumbnail: {
    fileName: String,
    path: String,
    size: String,
    addedOn: String
  }
});

module.exports = mongoose.model('Project', projectSchema);