'use strict';
var passport = require('passport');
var User = require('../../models/User');

exports.getLogin = function(req, res) {
  if (req.user) {
    return res.redirect('/project-manager/projects-list');
  } else {
    res.render('project-manager/auth/login');
  }
}

exports.postLogin = function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) {
      return next(err);
    }

    if (!user) {
      return res.redirect('/project-manager/login');
    }

    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
      res.redirect('/project-manager/projects-list');
    });
  })(req, res, next);
}

exports.getSignup = function(req, res) {
  if (req.user) {
    return res.redirect('/project-manager/projects-list');
  } else {
    res.render('project-manager/auth/signup');
  }
}

exports.postSignup = function(req, res) {
  var user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  });

  User.findOne({
    email: req.body.email
  }, function(err, existingUser) {
    if (existingUser) {
      return res.send('an user with that email already exists');
    }
    user.save(function(err) {
      if (err) {
        return next(err);
      }
      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }
        res.redirect('/project-manager/projects-list');
      });
    });
  });
}

exports.getLogout = function(req, res) {
  req.logout();
  res.redirect('/project-manager/login');
}