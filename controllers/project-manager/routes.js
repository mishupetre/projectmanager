'use strict';

var fs = require('fs');
var path = require('path');
var moment = require('moment');
var Project = require('../../models/Project');
var async = require('async');


// multer file upload configuration
var multer = require('multer');
var storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, 'uploads/');
  },
  filename: function(req, file, callback) {
    callback(null, Date.now() + '-' + file.originalname);
  }
});
var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 8 // 8MB File size limit
  }
}).array('upload', 2);


exports.getIndex = function(req, res) {
  res.redirect('/project-manager/projects-list');
}

exports.getProjectsList = function(req, res) {
  Project
    .find({})
    .sort('-addedOn')
    .exec(function(err, projects) {
      if (err) {
        console.log(err);
        return res.send('Failed to retrieve the projects list.');
      }
      res.render('project-manager/projects/list', {
        projects: projects
      });
    });
}

exports.getAddProject = function(req, res) {
  res.render('project-manager/projects/add');
}

exports.postAddProject = function(req, res) {
  upload(req, res, function(err) {
    if (err) {
      if (err.code === 'LIMIT_FILE_SIZE') {
        return res.send('The file you wanted to upload is too big.');
      }

      return res.send('Something happened when trying to upload your file.');
    }

    var project = new Project({
      name: req.body.name,
      category: req.body.category,
      description: req.body.description,
      fileName: req.files[0].filename,
      path: '/' + req.files[0].path,
      size: req.files[0].size,
      addedOn: moment().format(),
      thumbnail: {
        fileName: req.files[1].filename,
        path: '/' + req.files[1].path,
        size: req.files[1].size,
        addedOn: moment().format()
      }
    });

    project.save(function(err) {
      if (err) {
        console.log(err);
        return res.send('Something happened when trying to save your new project.')
      }

      res.redirect('/project-manager/projects-list');
    });
  });
}

exports.putProjectView = function(req, res) {
  Project.findByIdAndUpdate(req.params.id, {
    name: req.body.name,
    category: req.body.category,
    description: req.body.description,
    addedOn: moment().format()
  }, {
    new: true
  }, function(err, project) {
    if (err) {
      console.log(err);
      return res.send('Failed to update project ' + project.name);
    }
    res.send(project);
  });
}

exports.getProjectView = function(req, res) {
  Project.findById(req.params.id, function(err, project) {
    if (err) {
      console.log(err);
      return res.send('Failed to retrieve the requested project.');
    }
    res.render('project-manager/projects/view', {
      project: project
    });
  });
}

exports.deleteProject = function(req, res) {
  Project.findById(req.params.id, function(err, project) {
    if (err) {
      console.log(err);
      return res.send('Project couldn\'t be found');
    }

    var imagePath = './' + project.path;
    var thumbnailPath = './' + project.thumbnail.path;

    async.parallel([
      function(callback) {
        project.remove(callback);
      },
      function(callback) {
        fs.unlink(imagePath, callback);
      },
      function(callback) {
        fs.unlink(thumbnailPath, callback);
      }
    ], function(err) {
      if (err) {
        console.log(err);
        return res.send('Something happened when trying to delete your project');
      }

      res.send('Project ' + project.name + ' deleted.');
    });
  });
}