'use strict';

var Project = require('../models/Project');
var nodemailer = require('nodemailer');

exports.getIndex = function(req, res) {
  Project.find({}).
  sort('-addedOn').
  exec(function(err, projects) {
    if (err) {
      console.log(err);
      return res.send('Failed to retrieve the projects list.');
    }
    res.render('public-site/index', {
      projects: projects
    });
  });
}

exports.postContact = function(req, res) {
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.GMAIL_USER,
      pass: process.env.GMAIL_PASS
    }
  });

  console.log(process.env.GMAIL_USER);

  var mailOptions = {
    from: '"YM Site Message" <testnodejsmailer@gmail.com>',
    to: 'julia.matyashevska@gmail.com',
    subject: req.body.subject,
    html: 'Hi, Yuliya! Somebody sent you a message from your website:' +
      '<br><br>' +
      `<br><b>From:</b> ${req.body.name}` +
      `<br><b>Email:</b> <a href="mailto:${req.body.email}">${req.body.email}</a>` +
      `<br><b>Subject:</b> ${req.body.subject}` +
      `<br><b>Message:</b> ${req.body.message}`
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log('Something bad happened when trying to send the email.');
      return res.send('Something bad happened, your message couldn\'t be sent. Please try again.');
    }
    console.log('Email sent successfully.')
    res.send('Your message was sent successfully.');
  });
}